AntiFeatures:UpstreamNonFree
Categories:Games
License:GPLv3+
Web Site:http://nds4droid.com
Source Code:http://sourceforge.net/p/nds4droid/code
Issue Tracker:http://sourceforge.net/p/nds4droid/tickets
Donate:http://jeffq.com/blog/nds4droid

Auto Name:nds4droid
Summary:Nintendo DS emulator
Description:
nds4droid is a Nintendo DS emulator, based on DeSmuME. It is currently in its
infancy but does support most of the features one would expect in an emulator.
Runs on x86 natively.

Unlike official version this build does not support RAR archives.
.

Repo Type:git
Repo:git://git.code.sf.net/p/nds4droid/code

Build:13,13
    commit=release13
    target=android-15
    rm=jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/armeabi-v7a/libc.so
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff.patch
    buildjni=yes

Build:21,21
    commit=release21
    target=android-16
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:23,23
    commit=release23
    target=android-16
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:25,25
    commit=release25
    target=android-16
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:26,26
    commit=release26
    target=android-16
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:27,27
    commit=release27
    target=android-17
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:28,28
    commit=release28
    target=android-17
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:29,29
    commit=release29
    target=android-17
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:30,30
    commit=release30
    target=android-17
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:40,40
    commit=release40
    target=android-17
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=rm -rf jni/desmume/src/android/7z/CPP/7zip/Archive/obj/local/*
    buildjni=yes

Build:42,423
    commit=release42
    forcevercode=yes
    extlibs=acra/acra-4.2.3.jar
    patch=fix_stuff_2.patch
    prebuild=echo 'APP_ABI=armeabi-v7a' >> jni/Application.mk
    scanignore=jni/android-ndk-profiler
    scandelete=jni/desmume
    buildjni=yes

Build:44,441
    commit=release44
    forcevercode=yes
    patch=fix_stuff_2.patch
    prebuild=echo 'APP_ABI=x86' >> jni/Application.mk
    scanignore=jni/android-ndk-profiler
    scandelete=jni/desmume
    buildjni=yes

Build:44,443
    commit=release44
    forcevercode=yes
    patch=fix_stuff_2.patch
    prebuild=echo 'APP_ABI=armeabi-v7a' >> jni/Application.mk
    scanignore=jni/android-ndk-profiler
    scandelete=jni/desmume
    buildjni=yes

Build:45,451
    commit=release45
    forcevercode=yes
    patch=fix_stuff_2.patch
    prebuild=echo 'APP_ABI=x86' >> jni/Application.mk && \
        sed -i 's/IS_OUYA =.*/IS_OUYA = false;/' src/com/opendoorstudios/ds4droid/MainActivity.java
    scanignore=jni/android-ndk-profiler
    scandelete=jni/desmume,libs
    buildjni=yes

Build:45,453
    commit=release45
    forcevercode=yes
    patch=fix_stuff_2.patch
    prebuild=echo 'APP_ABI=armeabi-v7a' >> jni/Application.mk && \
        sed -i 's/IS_OUYA =.*/IS_OUYA = false;/' src/com/opendoorstudios/ds4droid/MainActivity.java
    scanignore=jni/android-ndk-profiler
    scandelete=jni/desmume,libs
    buildjni=yes

# +0: - (upstream)
# +1: x86
# +2: arm
# +3: armv7 (CV)
Archive Policy:6 versions
Auto Update Mode:None
Update Check Mode:Tags
Vercode Operation:%c*10 + 3
Current Version:45
Current Version Code:453

