AntiFeatures:Tracking,NonFreeNet
Categories:Navigation
License:Apache2
Web Site:https://github.com/microg/AppleWifiNlpBackend
Source Code:https://github.com/microg/AppleWifiNlpBackend
Issue Tracker:https://github.com/microg/AppleWifiNlpBackend/issues

Auto Name:AppleWifiNlpBackend
Summary:UnifiedNlp backend to resolve WiFi locations
Description:
[https://github.com/microg/android_packages_apps_UnifiedNlp UnifiedNlp] Backend
that uses Apple's service to resolve WiFi locations.
Location calculation is done onboard and wifi locations are cached to minimize data usage.
.

Repo Type:git
Repo:https://github.com/microg/AppleWifiNlpBackend.git

Build:1.0.0,1000
    commit=v1.0.0
    target=android-19
    srclibs=1:UnifiedNlpApi@v1.0.0,wire-runtime@wire-1.2.0,MapsAPI@v0.5
    prebuild=cp -a $$wire-runtime$$/src/main/java/* $$MapsAPI$$/src/* src/

Build:1.0.1,1010
    commit=v1.0.1
    target=android-19
    srclibs=1:UnifiedNlpApi@v1.0.0,wire-runtime@wire-1.2.0,MapsAPI@v0.5
    prebuild=cp -a $$wire-runtime$$/src/main/java/* $$MapsAPI$$/src/* src/

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:1010

