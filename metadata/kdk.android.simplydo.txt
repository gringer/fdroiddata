Categories:Office
License:GPLv3
Web Site:https://code.google.com/p/simply-do
Source Code:https://code.google.com/p/simply-do/source
Issue Tracker:https://code.google.com/p/simply-do/issues

Auto Name:Simply Do
Summary:Simple item list manager
Description:
Simply Do is a simple shopping/TODO/task list manager.
.

Repo Type:git-svn
Repo:https://simply-do.googlecode.com/svn;trunk=trunk;tags=tags

Build:0.9.1,1
    commit=18
    subdir=simply-do-app

Build:0.9.2,2
    commit=release-0.9.2
    subdir=simply-do-app
    target=android-10

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.9.2
Current Version Code:2

