Categories:Internet
License:GPLv3
Web Site:https://floens.github.io/Clover
Source Code:https://github.com/Floens/Clover
Issue Tracker:https://github.com/Floens/Clover/issues
Donate:https://floens.github.io/Clover/#donate
Bitcoin:1N7VtcNh8L8u4tF9CJ38GjnPbmxM4Vixi6

Auto Name:Clover
Summary:4chan image board browser
Description:
Clover is a browser for the popular [https://4chan.org/ 4chan] image board

Features include:
* Customizable boards - you can add more boards in the settings
* View images fast and save them
* Pin threads for easy access
* Watch threads to be notified of new posts and replies to you
* 4chan pass support
.

Repo Type:git
Repo:https://github.com/Floens/Clover.git

Build:v1.1.1,25
    commit=v1.1.1
    subdir=Clover/app
    gradle=yes

Build:v1.1.2,28
    commit=v1.1.2
    subdir=Clover/app
    gradle=yes

Build:v1.1.3,32
    commit=v1.1.3
    subdir=Clover/app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:v1.1.3
Current Version Code:32

