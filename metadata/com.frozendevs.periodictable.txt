Categories:Science & Education
License:GPLv3
Web Site:http://www.frozendevs.com
Source Code:https://github.com/Frozen-Developers/android-periodic-table
Issue Tracker:https://github.com/Frozen-Developers/android-periodic-table/issues

Auto Name:Periodic Table
Summary:Periodic table of the elements
Description:
Holo styled interactive periodic table with list of element properties and
isotopes based on Wikipedia's database.
.

Repo Type:git
Repo:https://github.com/Frozen-Developers/android-periodic-table

Build:1.0,2
    commit=b8d2a953a420eee9ab334c809afc28e0e3fca77a
    subdir=PeriodicTable
    gradle=main

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:2
