Categories:Wallpaper
License:MIT
Web Site:http://markusfisch.de
Source Code:https://github.com/markusfisch/WaveLinesWallpaper
Issue Tracker:https://github.com/markusfisch/WaveLinesWallpaper/issues

Auto Name:Wave Lines
Summary:Wavy wallpaper
Description:
No description available
.

Repo Type:git
Repo:https://github.com/markusfisch/WaveLinesWallpaper.git

Build:1.2.0,3
    commit=v1.2.0
    init=cat ant.properties >> project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2.0
Current Version Code:3

