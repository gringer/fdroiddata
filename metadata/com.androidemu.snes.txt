Disabled:Non-free license
Categories:Games
License:Unknown
Web Site:https://github.com/Pretz/SNesoid
Source Code:https://github.com/Pretz/SNesoid
Issue Tracker:

Auto Name:SNesoid
Summary:SNES emulator
Description:
No description available
.

Repo Type:git
Repo:https://github.com/Pretz/SNesoid.git

Build:2.1,68
    disable=non-commercial licence a9381085e0
    commit=unknown - see disabled
    subdir=SNesoid
    prebuild=cd .. && \
        git submodule init && \
        git submodule update
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Static
Current Version:2.1
Current Version Code:68

