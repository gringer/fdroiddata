Categories:Office
License:Artistic2
Web Site:http://passwdsafe.sourceforge.net
Source Code:http://sourceforge.net/p/passwdsafe/code/ci/default/tree
Issue Tracker:http://sourceforge.net/projects/passwdsafe/support
Donate:http://passwdsafe.sourceforge.net

Auto Name:PasswdSafe
Summary:Popular easy-to-use and secure password manager
Description:
PasswdSafe is a port of the popular Password Safe (pwsafe) application to the
Android platform. Supports viewing and editing of Password Safe data files.
Files are stored by default in the root directory on the SD card. Just copy a
.psafe3 or .dat file to the card. Note: The SD card must be unmounted from the
PC for the files to be seen.
.

Repo Type:hg
Repo:http://hg.code.sf.net/p/passwdsafe/code

Build:4.6.1,40601
    commit=rel-4.6.1
    srclibs=BouncyCastle@r1rv51
    subdir=passwdsafe
    target=android-8
    extlibs=android/android-support-v4.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-151.jar libs/ && \
        rm -rf ../{releases,sync,test,lib-box,lib-play} && \
        rm libs/bcprov-jdk15-143.jar && \
        mv libs/android-support-v4.jar ../lib/libs/ && \
        sed -i '/key.store/d' ant.properties
    scanignore=lib/libs/android-support-v4.jar,passwdsafe/libs/bcprov-jdk15on-151.jar

Auto Update Mode:None
Update Check Mode:Tags
Current Version:4.6.1
Current Version Code:40601
