Categories:Navigation
License:NetBSD
Web Site:http://apps4av.com/avare-overview/
Source Code:https://github.com/apps4av/avare
Issue Tracker:https://github.com/apps4av/avare/issues

Name:Avare
Summary:Aviation map
Description:
Avare (pronounced "AvAir") is a free moving aviation map. Basic features
include online or offline A/FD info, approach plates and GPS moving map
on all FAA VFR sectionals, WACs and TACs, plus IFR low charts and airport
diagrams. Avare also enables manual browsing of all charts and all other
materials even without cell service or GPS. Current FAA weather METARs,
TAFs and TFRs are provided too.

Notice: This is not an FAA certified GPS. 
.

Repo Type:git
Repo:https://github.com/apps4av/avare

Build:5.6.7,187
    disable=not all source files are licensed
    commit=bb9dd655ea148f1b5586db1aac0f6532ce284815

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:5.6.7
Current Version Code:187

