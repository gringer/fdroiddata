Categories:Wallpaper
License:Apache2
Web Site:
Source Code:https://github.com/elsewhat/com.elsewhat.android.currentwallpaper
Issue Tracker:https://github.com/elsewhat/com.elsewhat.android.currentwallpaper/issues

Auto Name:Current Wallpaper
Summary:Display wallpapers as standalone
Description:
No description available
.

Repo Type:git
Repo:https://github.com/elsewhat/com.elsewhat.android.currentwallpaper.git

Build:1.0,2
    commit=f9cc

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:2

