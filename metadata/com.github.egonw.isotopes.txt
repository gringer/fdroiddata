Categories:Science & Education
License:GPLv3
Web Site:https://github.com/chemicalappspace/Isotopes
Source Code:https://github.com/chemicalappspace/Isotopes
Issue Tracker:https://github.com/chemicalappspace/Isotopes/issues

Auto Name:Isotopes
Summary:Isotope Information
Description:
Isotopes is an app with more then 3000 isotopes, both natural and synthetic, including
accurate mass information and natural abundance.
.

Repo Type:git
Repo:https://github.com/chemicalappspace/Isotopes

Build:2.0,2
    commit=2.0

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0
Current Version Code:2

